'use strict';

const FULL = 100;
const NORMAL_ATTACK_MAX = 5;
const SPECIAL_MULTIPLIER = 3;
const MAX_HEAL = 10;

const PLAYER_EVENT_STYLE = {
    backgroundColor: 'skyblue',
    color: 'navy',
};

const MONSTER_EVENT_STYLE = {
    backgroundColor: 'pink',
    color: 'darkred',
};

new Vue({
    el: "#app",
    data: {
        eventLog: [],
        gameInProgress: false,
        monster: {
            health: FULL,
            eventStyle: MONSTER_EVENT_STYLE,
        },
        player: {
            health: FULL,
            eventStyle: PLAYER_EVENT_STYLE,
        }
    },
    computed: {
        yourHealthbar() {
            return { width: this.player.health + '%' };
        },
        monstersHealthbar() {
            return { width: this.monster.health + '%' };
        },
    },
    methods: {
        logEvent(message, style) {
            this.eventLog.unshift({message, style});
        },
        getAmount(limit) {
            return Math.ceil(Math.random() * Math.ceil(limit));
        },
        performAttack(from, to, multiplier = 1) {
            let attack = multiplier * this.getAmount(NORMAL_ATTACK_MAX);
            if (this[to].health < attack) {
                attack = this[to].health;
            }
            this[to].health -= attack;
            this.logEvent(`${from} HITS ${to} FOR ${attack}`.toUpperCase(), this[from].eventStyle);
        },
        attack() {
            this.performAttack('player', 'monster');
            this.performAttack('monster', 'player');
            this.checkGameEnd();
        },
        specialAttack() {
            this.performAttack('player', 'monster', 3);
            this.performAttack('monster', 'player');
            this.checkGameEnd();
        },
        heal() {
            this.yourHeal();
            this.performAttack('monster', 'player');
            this.checkGameEnd();
        },
        yourHeal() {
            let amount = this.getAmount(MAX_HEAL);
            if ((amount + this.player.health) > FULL) {
                amount = FULL - this.player.health;
            }
            this.player.health += amount;
            this.logEvent(`PLAYER HEALS FOR ${amount}`, this.player.eventStyle);
        },
        startNewGame() {
            this.player.health = FULL;
            this.monster.health = FULL;
            this.eventLog.splice(0);
            this.gameInProgress = true;
        },
        giveUp() {
            this.player.health = 0;
            this.logEvent('PLAYER GIVES UP', this.player.eventStyle);
            this.checkGameEnd();
        },
        checkGameEnd() {
            if (this.player.health <= 0) {
                this.gameInProgress = false;
            }
            if (this.monster.health <= 0) {
                this.gameInProgress = false;
            }
        }
    },
});

